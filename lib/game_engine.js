const random_line = require("@scrum_project/random");

const startGame = () => {
  let word_temp = random_line.getRandomWord();
  let display_word_temp = "_ ".repeat(word_temp.length);
  display_word_temp = display_word_temp.substring(0, display_word_temp.length - 1); // remove last space
  console.log(word_temp)
  console.log(word_temp.length)
  console.log(display_word_temp)
  
  return {
    status: "RUNNING",
    word: word_temp,
    lives: 5,
    display_word: display_word_temp,
    guesses: []
  };
};

const getAllIndexes = (arr, val)  => {
  var indexes = [], i = -1;
  while ((i = arr.indexOf(val, i+1)) != -1){
      indexes.push(i);
  }
  return indexes;
}


const split_word = (word) =>{
  return word.split(" ");
}

const join_word = (word) =>{
  return word.join(" ");
}

const takeGuess = (game_state, guess) => {
  const indexes = getAllIndexes(game_state.word,guess);
  const guess_is_right = indexes.length >=1;

  if (game_state.guesses.includes(guess)){
    return game_state
  }

  if  (!guess_is_right)  {
      if (game_state.lives ==1){
        game_state.status="GAMEOVER";
        game_state.lives= 0;
        game_state.guesses.push(guess);
      }else{
        game_state.lives --;
        game_state.guesses.push(guess);  
      }
  }else {
        game_state.guesses.push(guess);
        temp_display = split_word(game_state.display_word);
        indexes.forEach(function(element) {
            temp_display[parseInt(element)] = guess;
        });
        game_state.display_word = join_word(temp_display);
        console.log(game_state.display_word.includes("_"))
        if (!game_state.display_word.includes("_")){
            game_state.status="WINNER";
        }
      }
  return game_state;
};

module.exports = {
  startGame,
  takeGuess
};

startGame()
