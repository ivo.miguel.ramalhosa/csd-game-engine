const GameEngine = require("../lib/game_engine");

describe("The Game Engine must", () => {
  test("start a new game with a random word", () => {
    let game_start = GameEngine.startGame()

    expect(game_start.lives).toBe(5)
    expect(game_start.word).not.toBe(null)
    expect(game_start.word.length).toBeGreaterThan(2)
    expect(game_start.display_word.length).toBe((2*game_start.word.length)-1)
  });

  test("update the guesses and lives when a wrong guess is given", () => {
    let game_state = GameEngine.startGame();
    game_state.word = "fujitsu";

    let new_game_state = GameEngine.takeGuess(game_state,"a");
    expect(new_game_state.lives).toBe(4);
    expect(new_game_state.guesses.length).toBeGreaterThan(0);
    expect(new_game_state.guesses).toContain("a");

  });

  test("update the guesses and there is no more lives when a wrong guess is given", () => {
    let game_state = GameEngine.startGame();
    game_state.word = "fujitsu";
    game_state.lives = 1;
    let new_game_state = GameEngine.takeGuess(game_state,"a");
    expect(new_game_state.lives).toBe(0);
    expect(new_game_state.status).toBe("GAMEOVER");
    expect(new_game_state.guesses.length).toBeGreaterThan(0);
  });

  test("update the guesses when a right guess is given", () => {
    let game_state = GameEngine.startGame();
    game_state.word = "fujitsu";
    game_state.display_word = "_ _ _ _ _ _ _";
    let new_game_state = GameEngine.takeGuess(game_state,"u");
    expect(new_game_state.lives).toBe(5);
    expect(new_game_state.guesses.length).toBeGreaterThan(0);
    expect(new_game_state.display_word).toContain("u")
    expect(new_game_state.display_word).toBe("_ u _ _ _ _ u")
    expect(new_game_state.status).toBe("RUNNING")
  });

  test("update the guesses when a right guess is given and guess all the word", () => {
    let game_state = GameEngine.startGame();
    game_state.word = "fujitsu";
    game_state.display_word = "f _ j i t s _";
    let new_game_state = GameEngine.takeGuess(game_state,"u");
    expect(new_game_state.lives).toBe(5);
    expect(new_game_state.guesses.length).toBeGreaterThan(0);
    expect(new_game_state.display_word).toContain("u")
    expect(new_game_state.display_word).toBe("f u j i t s u")
    expect(new_game_state.status).toBe("WINNER")
  });

  test("update the guesses when is done a repeated guess", () => {
    let game_state = GameEngine.startGame();
    game_state.word = "fujitsu";
    let new_game_state = GameEngine.takeGuess(game_state,"k");
    new_game_state2 = GameEngine.takeGuess(new_game_state,"k");

    expect(new_game_state2.lives).toBe(4);
    expect(new_game_state.guesses.length).toBe(1);
  });


});
